# babel-preset-rn-node-dcore

## Getting started

```bash
# Install dependencies
npm i babel-preset-rn-node-dcore --save-dev
```

1. Add the following in your `.babelrc`

```
{
  "presets": [
    // "babel-preset-react-native" or "babel-preset-expo",
    "babel-preset-rn-node-dcore"
  ]
}
```

2. Import `globals` in your `App.js` as described below:

App.js

```js
import 'babel-preset-rn-node-dcore/globals';
```


## License

MIT @ drdc